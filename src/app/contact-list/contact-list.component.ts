import { Component, OnInit } from '@angular/core';

import { ContactService } from '../contact/contact.service';

import { MatTableDataSource } from '@angular/material';

@Component({
  selector: 'app-contact-list',
  templateUrl: './contact-list.component.html',
  styleUrls: ['./contact-list.component.css']
})
export class ContactListComponent implements OnInit {
  contacts: Array<ContactInterface>;
  displayedColumns = ['name', 'email', 'company', 'city', 'country', 'job_history'];
  dataSource = null;

  applyFilter(filterValue: string) {
    filterValue = filterValue.trim(); // Remove whitespace
    filterValue = filterValue.toLowerCase(); // MatTableDataSource defaults to lowercase matches
    this.dataSource.filter = filterValue;
  }
  constructor(private contactService: ContactService) { }

  ngOnInit() {
    this.contactService.getJSON().subscribe(data => {
      this.contacts = data;
      this.dataSource = new MatTableDataSource(this.contacts);
    });
  }
}

export interface ContactInterface {
  name: string;
  email: string;
  company: string;
  city: string;
  country: string;
  job_history: Array<string>;
}
