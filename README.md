# RolePoint Coding exercise
Simple web app that allows a user to search through his contacts. It should present the user with a search box and a list of all the people who match the current search. When there is no search term it should display all the contacts.

## Technologies
Since I got the choice of the technologies and tools, I am going to use Angular 5

## Development server
Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Link
[Angular CLI README](https://github.com/angular/angular-cli/blob/master/README.md)
